import pyfirmata
from array import *
import time

def talk_with_arduino_at(new_serialport):
    global board
    board = pyfirmata.Arduino(new_serialport)

    global it
    it = pyfirmata.util.Iterator(board)
    it.start()

    time.sleep(1)
    print("")
    print("Set up arduino at serialport", new_serialport)
    print(" ")


def use_analog_pins(input_pins):
    global input_pin
    input_pin = input_pins

    print("Got the inputs ", input_pin)

    for i in input_pin:
        board.analog[i].enable_reporting()
        print("Enabled reporting on pin", i)
    print(" ")


def get_analog_input():
    analog_inputs = array("f", [])
    for i in input_pin:
        analog_input = board.analog[i].read()

        if analog_input is not None:
            analog_inputs.append(float(analog_input))
        else:
            analog_inputs.append(0.0)

    return analog_inputs


def map(x, original_min, original_max, new_min, new_max):
    mapped_thing = (x - original_min) * (new_max - new_min) / (original_max - original_min) + new_min
    return mapped_thing
